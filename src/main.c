
#include "tests.h"

int main() {
    test_alloc();
    test_alloc_and_free();
    test_alloc_3_and_free_2();
    test_expand_old_region();
    test_inpossible_expand();
    debug("ALL TESTS PASSED!!!\n");
}
