#define _GNU_SOURCE
#include "tests.h"
#define SMALL_HEAP 50
#define ALLC_1 150
#define ALLC_2 140
#define ALLC_3 200

void test_alloc(){
    debug("=============================\nTest #1:\n");
    debug("Trying to create heap, size %d byte\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Created heap successfully, size %d byte\n", HEAP_SIZE);
    debug("Trying to allocate %d byte\n", ALLC_1);
    void* allc = _malloc(ALLC_1);
    assert(allc);
    debug("Allocated successfully!\n");

    heap_term();
    debug("Test #1 passed successfully!\n=============================\n");
}

void test_alloc_and_free(){
    debug("=============================\nTest #2:\n");
    debug("Trying to create heap, size %d byte\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Created heap successfully, size %d byte\n", HEAP_SIZE);
    debug("1: Allocation of 2 blocks\n");
    debug("\tTrying to allocate %d byte\n", ALLC_1);
    void* allc1 = _malloc(ALLC_1);
    assert(allc1);
    debug("\tTrying to allocate %d byte\n", ALLC_2);
    void* allc2 = _malloc(ALLC_2);
    assert(allc2);
    debug("Allocated successfully!\n");

    debug("2: Removing first block\n");
    _free(allc1);
    assert(block_get_header(allc1)->is_free);
    assert(!block_get_header(allc2)->is_free);

    heap_term();
    debug("Test #2 passed successfully!\n=============================\n");
}

void test_alloc_3_and_free_2(){
    debug("=============================\nTest #3:\n");
    debug("Trying to create heap, size %d byte\n", HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Created heap successfully, size %d byte\n", HEAP_SIZE);
    debug("1: Allocation of 3 blocks\n");
    debug("\tTrying to allocate %d byte\n", ALLC_1);
    void* allc1 = _malloc(ALLC_1);
    assert(allc1);
    debug("\tTrying to allocate %d byte\n", ALLC_2);
    void* allc2 = _malloc(ALLC_2);
    assert(allc2);
    debug("\tTrying to allocate %d byte\n", ALLC_3);
    void* allc3 = _malloc(ALLC_3);
    assert(allc3);
    debug("Allocated successfully!\n");

    debug("2: Removing first and second blocks\n");
    _free(allc2);
    _free(allc1);
    assert(block_get_header(allc1)->is_free);
    assert(block_get_header(allc2)->is_free);
    assert((block_get_header(allc1)->capacity.bytes == ALLC_1 + size_from_capacity((block_capacity){.bytes=ALLC_2}).bytes));
    assert(!block_get_header(allc3)->is_free);
    heap_term();
    debug("Test #3 passed successfully!\n=============================\n");
}

void test_expand_old_region(){
    debug("=============================\nTest #4:\n");
    debug("Trying to create heap, size %d byte\n", SMALL_HEAP);
    void* heap = heap_init(SMALL_HEAP);
    assert(heap);
    debug("Created heap successfully, size %d byte\n", SMALL_HEAP);
    debug("\tTrying to allocate %d byte\n", ALLC_1);
    void* allc = _malloc(ALLC_1);
    assert(allc);
    debug("Allocated successfully!\n");

    heap_term();
    debug("Test #4 passed successfully!\n=============================\n");
}

void test_inpossible_expand(){
    debug("=============================\nTest #5:\n");

    debug("Creating region\n");
    void* pre_allocated = map_pages(HEAP_START, 8, MAP_FIXED);
    assert(pre_allocated);
    debug("Allocating to filled region\n");
    void* alloc_in_filled_ptr = _malloc(ALLC_1);
    assert(alloc_in_filled_ptr);
    assert(pre_allocated != alloc_in_filled_ptr);

    heap_term();
    debug("Test #5 passed successfully!\n=============================\n");
}
