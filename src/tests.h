#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096

extern void debug(const char* fmt, ... );
extern struct block_header* block_get_header(void* contents);
extern void* map_pages(void const* addr, size_t length, int additional_flags);

void test_alloc();
void test_alloc_and_free();
void test_alloc_3_and_free_2();
void test_expand_old_region();
void test_inpossible_expand();

